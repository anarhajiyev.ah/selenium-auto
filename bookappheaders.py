from selenium.webdriver.chrome.service import Service
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

service = Service(executable_path='./chromedriver.exe')
driver = webdriver.Chrome(service=service)

# landing on the main page

def main_page():
    driver.get("https://www.barnesandnoble.com/")
    assert(driver.title)

main_page()

# verifying each page tag is clickable and loading and confirms the tag title

tags=["Books","Fiction","Nonfiction","eBooks","Audiobooks","Teens & YA","Kids","Toys & Games","Stationery & Gifts","Music & Movies"]

for tag in tags: 
    driver.find_element(By.ID,"rhfCategoryFlyout_"+tag).click()
    assert(driver.title)
      
driver.quit()
