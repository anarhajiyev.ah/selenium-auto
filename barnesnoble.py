from selenium.webdriver.chrome.service import Service
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

service = Service(executable_path='./chromedriver.exe')
driver = webdriver.Chrome(service=service)

#Test Successful landing on main page
# Given barnes and nobles url
# When i type url into the browser
#Then I successfully land on the main page

#initialize
def successful_landing_on_the_page():
    driver.get("https://www.barnesandnoble.com/")
    assert(driver.title)


successful_landing_on_the_page()

input_box = driver.find_element(By.CLASS_NAME, "rbt-input-main")
input_box.send_keys("Harry Potter")
input_box.send_keys(Keys.RETURN)
time.sleep(5)

#finalizing
driver.quit()

# OOP - inheritance
# selenium - wait times: explicit wait time, implicit wait time, flex wait time


#implicit wait  time 
# driver.get("http://somedomain/url_that_delays_loading")
# try:
#     element = WebDriverWait(driver, 10).until(
#         EC.presence_of_element_located((By.ID, "myDynamicElement"))
#     )
# finally:
#     driver.quit()
